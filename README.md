# zoo-management
##III-ZOO動物園管理系統
+ 製作框架：Vue.js  
+ 目的：熟悉vue-vuetify-vuex之應用，熟悉git之commit應用

### 系統功能
+ 首頁(home)：動物分類
+ 關於(about):描述關於III-ZOO之詳細資料
+ 哺乳類(Mammal)：陳列所有哺乳類動物成員之詳細資料，另附有新增、修改、刪除之功能
+ 鳥類(Bird)：陳列所有鳥類動物成員之詳細資料，功能同上
+ 爬蟲類(Reptile)：陳列所有鳥類動物成員之詳細資料，功能同上
+ 魚類(Fish)：陳列所有魚類動物成員之詳細資料，功能同上
+ 新增功能：可讓使用者自行新增該類動物資料，分別有姓名、負責人、性別、年齡、備註等可供填寫
+ 編輯功能：可讓使用者針對上述資料舊筆資料做資料修正
+ 刪除功能：可讓使用者自行刪除舊資料
+ 資料超出五筆時，顯示分頁，分頁可正確切換頁面

### 框架實際應用
+ App.vue:存取個頁面均可看見之物件
+ Store.js:存取動物清單至store.state
+ Vuetify:介面設計之套件
+ router.js:明確定義路由規則


### Compiles and hot-reloads for development
```
npm run serve
```
### Compiles and minifies for production
```
npm run build
```
### Run your tests
```
npm run test
```
### Lints and fixes files
```
npm run lint
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

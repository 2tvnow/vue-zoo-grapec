import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/mammal',
      name: 'mammal',
      component: () => import(/* webpackChunkName: "about" */ './views/Mammal.vue')
    },
    {
      path: '/bird',
      name: 'bird',
      component: () => import(/* webpackChunkName: "about" */ './views/Bird.vue')
    },
    {
      path: '/reptile',
      name: 'reptile',
      component: () => import(/* webpackChunkName: "about" */ './views/Reptile.vue')
    },
    {
      path: '/fish',
      name: 'fish',
      component: () => import(/* webpackChunkName: "about" */ './views/Fish.vue')
    },
  ]
})

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    animals: [
      {
        name: 'GrapeC',
        manager: 'Amy',
        gender: '女生',
        age: 23,
        remark: '["慵懶", "兇猛", "愛吃"]'
      },
      {
        name: 'Yao',
        manager: 'Tony',
        gender: '男生',
        age: 24,
        remark: '["熱心", "愛吃"]'
      },
      {
        name: '佳志',
        manager: 'Uln',
        gender: '男生',
        age: 25,
        remark: '["機智", "溫和"]'
      },
      {
        name: '千層',
        manager: 'Uln',
        gender: '女生',
        age: 25,
        remark: '["活潑", "機智", "溫和"]'
      },
      {
        name: '大盧',
        manager: 'Uln',
        gender: '男生',
        age: 26,
        remark: '["活潑", "隨和"]'
      },
      {
        name: 'Edward',
        manager: 'Uln',
        gender: '男生',
        age: 23,
        remark: '["機智", "隨和"]'
      },
      {
        name: 'Tony',
        manager: 'Uln',
        gender: '男生',
        age: 23,
        remark: '["親人", "隨和","機智"]'
      },
      {
        name: '+1000',
        manager: 'Uln',
        gender: '男生',
        age: 26,
        remark: '["活潑", "好客"]'
      },
      {
        name: '大叔',
        manager: 'Uln',
        gender: '男生',
        age: 55,
        remark: '["好客", "親人"]'
      },
    ]
  },
  mutations: {
    deleteItem (state, payload) {
      const index = this.state.animals.indexOf(payload);
      state.animals.splice(index, 1)
    },

    cleanData () {
      return new Promise((resolve) => {
        this.state.animals = []
        resolve('cleanData successfully')
      })
    },

    generateDataset () {
      return new Promise((resolve) => {
        this.state.animals = [
          {
            name: 'GrapeC',
            manager: 'Amy',
            gender: '女生',
            age: 23,
            remark: '["慵懶", "兇猛", "愛吃"]'
          },
          {
            name: 'Yao',
            manager: 'Tony',
            gender: '男生',
            age: 24,
            remark: '["熱心", "愛吃"]'
          },
          {
            name: '佳志',
            manager: 'Uln',
            gender: '男生',
            age: 25,
            remark: '["機智", "溫和"]'
          },
          {
            name: '千層',
            manager: 'Uln',
            gender: '女生',
            age: 25,
            remark: '["活潑", "機智", "溫和"]'
          },
          {
            name: '大盧',
            manager: 'Uln',
            gender: '男生',
            age: 26,
            remark: '["活潑", "隨和"]'
          },
          {
            name: 'Edward',
            manager: 'Uln',
            gender: '男生',
            age: 23,
            remark: '["機智", "隨和"]'
          },
          {
            name: 'Tony',
            manager: 'Uln',
            gender: '男生',
            age: 23,
            remark: '["親人", "隨和","機智"]'
          },
          {
            name: '+1000',
            manager: 'Uln',
            gender: '男生',
            age: 26,
            remark: '["活潑", "好客"]'
          },
          {
            name: '大叔',
            manager: 'Uln',
            gender: '男生',
            age: 55,
            remark: '["好客", "親人"]'
          },
        ]
        resolve('generateDataset successfully')
      })
    }
  },

  actions: {
    async initialize (context) {
    await context.commit('cleanData')
    await context.commit('generateDataset')
    }
  }
})
